/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.6.40 : Database - shiro
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`shiro` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shiro`;

/*Table structure for table `permission` */

DROP TABLE IF EXISTS `permission`;

CREATE TABLE `permission` (
  `pid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `permission` */

insert  into `permission`(`pid`,`name`,`url`) values ('1','超级管理员权限','/*'),('b7a1615c-edcb-4b7d-a38e-676f0ab56d0e','查询所有菜单','/permission/all'),('031199df-ae9f-45fb-a065-5c99f31c0d6d','添加菜单','/permission/add'),('42773ac4-8e63-495b-b6ca-323664eb3d44','查询所有菜单(分页)','/permission/list'),('69c378f7-486a-43d1-a141-d00fdcf802b3','无任何权限','/noAnyPermissions'),('d866e6e0-1c33-47ba-a338-249ff833b734','修改菜单','/permission/update'),('c637e0ca-1b4e-4bce-9bcb-8dbc3dce4081','添加角色','/role/add'),('20806e0e-e56c-4123-875c-cbce370c5057','查询所有角色','/role/list'),('57e04ffb-02ee-4927-a26e-bb6343e5175c','修改角色','/role/update'),('a03cf77d-1e20-405e-84a4-5818b1696521','添加用户','/user/add'),('ed62f02c-a29a-4c36-ae4a-4e3bbf885506','查询用户','/user/role/list'),('a8eeed22-9d12-496a-a516-53b271d6ea6e','修改用户','/user/update'),('23a5042f-c278-4709-b98f-085ee9f93497','修改用户状态','/user/updateStatus'),('4c53e03b-47c6-46b4-bcea-9af3ec538a7c','重置用户密码','/user/resetPassword'),('66b703cd-8b45-4492-98e5-7832ed30d1bf','查询指定角色的菜单权限','/role/permission');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `rid` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`rid`,`name`) values ('1','admin'),('09e81a07-64fb-d718-68c1-51db401e6f66','普通管理员'),('cfcabc4e-1757-4c5b-c232-48b28ba8eaa8','会员'),('7efb7278-99f9-7c2b-22b5-a5f7cf23be53','游客');

/*Table structure for table `role_permission` */

DROP TABLE IF EXISTS `role_permission`;

CREATE TABLE `role_permission` (
  `id` varchar(255) NOT NULL,
  `pid` varchar(255) DEFAULT NULL,
  `rid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `role_permission` */

insert  into `role_permission`(`id`,`pid`,`rid`) values ('796de397-b89f-49c8-b169-89ddbff6c3fe','1','1'),('e61afb2b-d499-4941-90c9-6fcde4a8dcc5','69c378f7-486a-43d1-a141-d00fdcf802b3','1'),('44d94794-ae3d-4fa1-9f40-fe96554f053f','4c53e03b-47c6-46b4-bcea-9af3ec538a7c','09e81a07-64fb-d718-68c1-51db401e6f66'),('f4a2c86d-5d3b-48f1-b9d9-9ef4974aa572','a8eeed22-9d12-496a-a516-53b271d6ea6e','cfcabc4e-1757-4c5b-c232-48b28ba8eaa8'),('fedd85fc-2d23-43dd-99ca-8a8215eb9a56','69c378f7-486a-43d1-a141-d00fdcf802b3','7efb7278-99f9-7c2b-22b5-a5f7cf23be53'),('4739e22f-7cd5-4e05-98ab-7df5a91efecd','20806e0e-e56c-4123-875c-cbce370c5057','7efb7278-99f9-7c2b-22b5-a5f7cf23be53'),('8cecfbfd-f9ce-4b83-bf92-f0c78a0d82db','ed62f02c-a29a-4c36-ae4a-4e3bbf885506','7efb7278-99f9-7c2b-22b5-a5f7cf23be53'),('953f9789-3f67-4873-814c-e97fd9dbac5f','23a5042f-c278-4709-b98f-085ee9f93497','cfcabc4e-1757-4c5b-c232-48b28ba8eaa8'),('54780fb9-7288-4d63-90ec-ef54d8969e0a','4c53e03b-47c6-46b4-bcea-9af3ec538a7c','cfcabc4e-1757-4c5b-c232-48b28ba8eaa8'),('42d55154-8bf1-49c1-aa4c-97310d260774','42773ac4-8e63-495b-b6ca-323664eb3d44','7efb7278-99f9-7c2b-22b5-a5f7cf23be53'),('4c2e55e6-df2a-49f7-952a-2afa0b541943','a03cf77d-1e20-405e-84a4-5818b1696521','cfcabc4e-1757-4c5b-c232-48b28ba8eaa8'),('ac395437-0fb5-4e2e-ae1b-762273d391d2','ed62f02c-a29a-4c36-ae4a-4e3bbf885506','cfcabc4e-1757-4c5b-c232-48b28ba8eaa8'),('2b32f82a-b469-43e7-b928-c0aafe4c05f8','b7a1615c-edcb-4b7d-a38e-676f0ab56d0e','7efb7278-99f9-7c2b-22b5-a5f7cf23be53'),('8561a930-c33f-4847-86bf-0d1f4dbe9163','23a5042f-c278-4709-b98f-085ee9f93497','09e81a07-64fb-d718-68c1-51db401e6f66'),('ab315356-3f6d-42e2-895f-a9bdcdabd842','a8eeed22-9d12-496a-a516-53b271d6ea6e','09e81a07-64fb-d718-68c1-51db401e6f66');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `uid` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `NewIndex1` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`uid`,`password`,`username`,`status`) values ('1','123456','admin',1),('274280fc-5524-28ab-0dcb-b8a539083242','123456','zhangsan',1),('3468999f-5c94-e32b-24d2-8d8d5b8262d2','123456','test',1);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` varchar(255) NOT NULL,
  `rid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

insert  into `user_role`(`id`,`rid`,`uid`) values ('01ca84fa-581f-4d0f-96b8-36af435fb09a','1','1'),('1325a1a2-d9e2-4822-9535-abbfb755d8dd','7efb7278-99f9-7c2b-22b5-a5f7cf23be53','3468999f-5c94-e32b-24d2-8d8d5b8262d2'),('63b2a5cc-bfa4-440c-8dc9-2c34eb00fc15','09e81a07-64fb-d718-68c1-51db401e6f66','3468999f-5c94-e32b-24d2-8d8d5b8262d2'),('166b9038-f5df-49e0-a72b-2267f674704c','7efb7278-99f9-7c2b-22b5-a5f7cf23be53','274280fc-5524-28ab-0dcb-b8a539083242');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

package com.dh.entity; 
/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午11:52:55 
 * @Description 类说明 :
 */

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "Role")
@Entity
public class Role implements Serializable{

	private static final long serialVersionUID = 1954L;

	@Id
	private String rid;
	
	private String name;
	
	public Role() {
		super();
	}

	public Role(String rid, String name) {
		super();
		this.rid = rid;
		this.name = name;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

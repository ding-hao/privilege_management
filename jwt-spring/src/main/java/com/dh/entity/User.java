package com.dh.entity; 
/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午11:52:47 
 * @Description 类说明 :
 */

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "User")
@Entity
public class User implements Serializable{

	private static final long serialVersionUID = 1845L;

	@Id
	private String uid;
	
	@Column(unique=true, nullable=false)
	private String username;
	
	@Column(unique=true, nullable=false)
	private String password;
	
	private Integer status;
	

	public User() {
		super();
	}

	public User(String uid, String username, String password) {
		super();
		this.uid = uid;
		this.username = username;
		this.password = password;
	}

	public User(String uid, String username, String password, Integer status) {
		super();
		this.uid = uid;
		this.username = username;
		this.password = password;
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}

package com.dh.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月17日 下午2:38:14 
 * @Description 类说明 : 日志实体类
 */

@Table(name = "SysLog")
@Entity
public class SysLog implements Serializable {

	private static final long serialVersionUID = 6151393423006132209L;

	@Id
	private String id;

	@Column(nullable=false)
    private String username;

    private String ip;

    @Column(nullable=false)
    private String uri;

    private String params;

    private String httpMethod;

    private String classMethod;

    // 从注解@SysLogs中获取
    private String actionName;

    private Date createDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	public String getClassMethod() {
		return classMethod;
	}

	public void setClassMethod(String classMethod) {
		this.classMethod = classMethod;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "SysLog [id=" + id + ", username=" + username + ", ip=" + ip + ", uri=" + uri
				+ ", params=" + params + ", httpMethod=" + httpMethod + ", classMethod=" + classMethod + ", actionName="
				+ actionName + ", createDate=" + createDate + "]";
	}
}

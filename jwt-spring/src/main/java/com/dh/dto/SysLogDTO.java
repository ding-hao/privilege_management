package com.dh.dto;

import java.io.Serializable;
import java.util.Date;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月17日 下午3:42:30 
 * @Description 类说明 :
 */
public class SysLogDTO implements Serializable{

	private static final long serialVersionUID = 7705090953333850281L;
	
	private String ip;
	
	private String uri;
	
	private String username;
	
	private String actionName;
	
	private Date startTime;
	
	private Date endTime;
	
	private Integer page;
	
	private Integer pagesize;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getActionName() {
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPagesize() {
		return pagesize;
	}

	public void setPagesize(Integer pagesize) {
		this.pagesize = pagesize;
	}

	@Override
	public String toString() {
		return "SysLogDTO [ip=" + ip + ", uri=" + uri + ", username=" + username + ", actionName=" + actionName
				+ ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}
	
}

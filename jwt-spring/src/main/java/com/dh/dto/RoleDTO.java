package com.dh.dto;

import java.io.Serializable;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月5日 下午5:39:26 
 * @Description 类说明 :
 */
public class RoleDTO implements Serializable{

	private static final long serialVersionUID = 232183980227070215L;
	
	private String rid;
	
	private String name;
	
	private String pids;

	public String getPids() {
		return pids;
	}

	public void setPids(String pids) {
		this.pids = pids;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "RoleDTO [rid=" + rid + ", name=" + name + ", pids=" + pids + "]";
	}
	
}

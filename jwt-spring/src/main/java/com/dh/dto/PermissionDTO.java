package com.dh.dto;

import java.io.Serializable;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月6日 上午10:02:37 
 * @Description 类说明 :
 */
public class PermissionDTO implements Serializable{

	private static final long serialVersionUID = 7064676013560174879L;

	private String pid;
	
	private String name;
	
	private String url;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return "PermissionDTO [pid=" + pid + ", name=" + name + ", url=" + url + "]";
	}
	
}

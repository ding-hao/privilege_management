package com.dh.dto;

import java.io.Serializable;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月5日 下午5:23:17 
 * @Description 类说明 :
 */
public class UserDTO implements Serializable{

	private static final long serialVersionUID = -2683782401421991500L;

	private String uid;
	
	private String username;
	
	private String password;
	
	private Integer status;
	
	private String rids;

	public String getRids() {
		return rids;
	}

	public void setRids(String rids) {
		this.rids = rids;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "UserDTO [uid=" + uid + ", username=" + username + ", password=" + password + ", status=" + status
				+ ", rids=" + rids + "]";
	}
}

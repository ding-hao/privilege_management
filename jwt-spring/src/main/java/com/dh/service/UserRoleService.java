package com.dh.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.dh.entity.Role;
import com.dh.entity.UserRole;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:27:28
 * @Description 类说明 :
 */
public interface UserRoleService {

	/**
	 * 
	 * @Title: findByUid
	 * @author
	 * @Description: 按用户id查询用户所拥有的角色
	 * @param uid
	 * @return
	 * @return List<Role>
	 */
	List<Role> findByUid(String uid);

	/**
	 * 
	 * @Title: forUserToAddRole
	 * @author  
	 * @Description: 为用户添加角色
	 * @param uid
	 * @param rids
	 * @return
	 * @return List<UserRole>
	 */
	List<UserRole> forUserToAddRole(String uid, String rids);

	/**
	 * 
	 * @Title: forUserToUpdateRole
	 * @author  
	 * @Description: 为用户修改角色
	 * @param uid
	 * @param rids
	 * @return
	 * @return List<UserRole>
	 */
	List<UserRole> forUserToUpdateRole(String uid, String rids);

	/**
	 * 
	 * @Title: findUserRoleList
	 * @author  
	 * @Description: 根据用户id查询用户角色信息
	 * @param option
	 * @param page
	 * @param pagesize
	 * @param sort
	 * @param order
	 * @return
	 * @return Page<UserRole>
	 */
	Page<UserRole> findUserRoleList(String option, Integer page, Integer pagesize, String sort, String order);

}

package com.dh.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.dh.entity.Permission;
import com.dh.entity.RolePermission;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:38:58 
 * @Description 类说明 :
 */
public interface RolePermissionService {

	/**
	 * 
	 * @Title: findByRid
	 * @author  
	 * @Description: 按角色id查询权限菜单列表
	 * @param rid
	 * @return
	 * @return List<Permission>
	 */
	List<Permission> findByRid(String rid);

	
	/**
	 * 
	 * @Title: forRoleAddPermission
	 * @author  
	 * @Description: 为角色添加权限菜单
	 * @param rid
	 * @param pids
	 * @return
	 * @return List<RolePermission>
	 */
	List<RolePermission> forRoleAddPermission(String rid, String pids);


	/**
	 * 
	 * @Title: forRoleUpdatePermission
	 * @author  
	 * @Description: 为角色修改权限菜单
	 * @param rid
	 * @param pids
	 * @return
	 * @return List<RolePermission>
	 */
	List<RolePermission> forRoleUpdatePermission(String rid, String pids);


	/**
	 * 
	 * @Title: findRolePermissionList
	 * @author  
	 * @Description: 根据角色查询菜单
	 * @param option
	 * @param page
	 * @param pagesize
	 * @param sort
	 * @param order
	 * @return
	 * @return Page<RolePermission>
	 */
	Page<RolePermission> findRolePermissionList(String option, Integer page, Integer pagesize, String sort,
			String order);

}

package com.dh.service;

import org.springframework.data.domain.Page;

import com.dh.dto.UserDTO;
import com.dh.entity.User;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:02:40 
 * @Description 类说明 :
 */
public interface UserService {

	/**
	 * 
	 * @Title: findByUsername
	 * @author  
	 * @Description: 通过用户名查询用户
	 * @param username
	 * @return
	 * @return User
	 */
	User findByUsername(String username);

	/**
	 * 
	 * @Title: findUserList
	 * @author  
	 * @Description: 通过用户名查询用户
	 * @param option
	 * @param order 
	 * @param sort 
	 * @param pagesize 
	 * @param page 
	 * @return
	 * @return List<User>
	 */
	Page<User> findUserList(String option, Integer page, Integer pagesize, String sort, String order);

	/**
	 * 
	 * @Title: addUser
	 * @author  
	 * @Description: 添加用户
	 * @param userDTO
	 * @return void
	 */
	User addUser(UserDTO userDTO);

	/**
	 * 
	 * @Title: updateUser
	 * @author  
	 * @Description: 修改用户信息
	 * @param userDTO
	 * @return
	 * @return User
	 */
	User updateUser(UserDTO userDTO);

	/**
	 * 
	 * @Title: updateUserStatus
	 * @author  
	 * @Description: 修改用户状态
	 * @param status
	 * @return
	 * @return User
	 */
	User updateUserStatus(String uid);

	/**
	 * 
	 * @Title: resetUserPassword
	 * @author  
	 * @param password 
	 * @Description: 重置用户密码
	 * @return
	 * @return User
	 */
	User resetUserPassword(String uid, String password);

	
	/**
	 * 
	 * @Title: removeUser
	 * @author  
	 * @Description: 根据用户id删除用户
	 * @param uid
	 * @return void
	 */
	void removeUser(String uid);

	

	

}

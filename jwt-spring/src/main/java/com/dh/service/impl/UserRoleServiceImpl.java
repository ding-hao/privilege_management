package com.dh.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dh.core.common.enums.BizExceptionEnum;
import com.dh.core.common.exception.BussinessException;
import com.dh.core.common.utils.CollectionUtil;
import com.dh.dao.RoleDao;
import com.dh.dao.UserRoleDao;
import com.dh.entity.Role;
import com.dh.entity.UserRole;
import com.dh.service.UserRoleService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:27:50 
 * @Description 类说明 :
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private RoleDao roleDao;
	
	@Override
	public List<Role> findByUid(String uid) {
		List<Role> roleList = new ArrayList<Role>();
		List<UserRole> ridList = userRoleDao.findByUid(uid);
		if(!CollectionUtils.isEmpty(ridList)) {
			for (UserRole userRole : ridList) {
				roleList.add(roleDao.findById(userRole.getRid()).get());
			}
			return roleList;
		}
		return null;
	}

	@Override
	public List<UserRole> forUserToAddRole(String uid,String rids) {
		List<UserRole> userRoles = userRoleDao.findByUid(uid);
		List<String> ridList = Arrays.asList(rids.split(","));
		UserRole userRole = null;
		//用户已存在角色
		if(!userRoles.isEmpty()) {
			Set<String> ridSet = userRoles.stream().map(UserRole::getRid).collect(Collectors.toSet());
			Collection<String> removeExist = CollectionUtil.removeExist(ridSet, ridList);
			for (String rid : removeExist) {
				if(StringUtils.isNotBlank(rid)) {
					userRole = new UserRole(UUID.randomUUID().toString(),uid,rid);
					UserRole saveAndFlush = userRoleDao.saveAndFlush(userRole);
					// 报存成功
					if(saveAndFlush != null) {
						userRoles.add(saveAndFlush);
					}
				}
			}
		}else {//用户不存在其他角色
			for (String rid : ridList) {
				if(StringUtils.isNotBlank(rid)) {
					userRole =  new UserRole(UUID.randomUUID().toString(),uid,rid);
					UserRole saveAndFlush = userRoleDao.saveAndFlush(userRole);
					// 报存成功
					if(saveAndFlush != null) {
						userRoles.add(saveAndFlush);
					}
				}
			}
		}
		return userRoles;
	}

	@Transactional
	@Override
	public List<UserRole> forUserToUpdateRole(String uid, String rids) {
		if("1".equals(uid)) {
			throw new BussinessException(BizExceptionEnum.NOT_UPDATE_ADMIN);
		}
		
		// 传递的用户id不能为空
		if(StringUtils.isNotBlank(uid)) {
			List<UserRole> userRoles = userRoleDao.findByUid(uid);
			// 修改前的用户角色
			if(!userRoles.isEmpty()) {
				userRoleDao.deleteByUid(uid);
				List<String> ridList = Arrays.asList(rids.split(","));
				UserRole userRole = null;
				for (String rid : ridList) {
					if(StringUtils.isNotBlank(rid)) {
						userRole = new UserRole(UUID.randomUUID().toString(),uid,rid);
						userRoleDao.save(userRole);
					}
				}
				// 用户不存在，去添加用户
			}else {
				throw new BussinessException(BizExceptionEnum.USER_NO_PRESENT);
			}
			return userRoleDao.findByUid(uid);
		}
		return null;
	}

	@Override
	public Page<UserRole> findUserRoleList(String option, Integer page, Integer pagesize, String sort, String order) {
		UserRole userRole = new UserRole();
		if(StringUtils.isNotBlank(option)) {
			userRole.setUid(option);
		}
		Example<UserRole> example = Example.of(userRole);
		
		PageRequest pageable = null;
		if(StringUtils.isNotBlank(sort)) {
			Direction direction = Direction.ASC.toString().equals(order) ? Direction.ASC : Direction.DESC;
			Sort Sort = new Sort(direction, sort);
			pageable = PageRequest.of(page, pagesize, Sort);
		}else {
			pageable = PageRequest.of(page, pagesize);
		}
		
		return userRoleDao.findAll(example, pageable);
	}
	
	

}

package com.dh.service.impl;

import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.dh.core.common.enums.BizExceptionEnum;
import com.dh.core.common.exception.BussinessException;
import com.dh.dao.UserDao;
import com.dh.dao.UserRoleDao;
import com.dh.dto.UserDTO;
import com.dh.entity.User;
import com.dh.service.UserService;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:03:20
 * @Description 类说明 :
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private UserRoleDao userRoleDao;

	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username).get(0);
	}

	@Override
	public Page<User> findUserList(String option, Integer page, Integer pagesize, String sort, String order) {
		User user = new User();
		Example<User> example = Example.of(user);
		if (StringUtils.isNotBlank(option)) {
			user.setUsername(option);
		}
		PageRequest pageable = null;
		if (StringUtils.isNotBlank(sort)) {
			Direction direction = Direction.ASC.toString().equals(order) ? Direction.ASC : Direction.DESC;
			Sort Sort = new Sort(direction, sort);
			pageable = PageRequest.of(page, pagesize, Sort);
		} else {
			pageable = PageRequest.of(page, pagesize);
		}
		return userDao.findAll(example, pageable);
	}

	@Override
	public User addUser(UserDTO userDTO) {
		User user = new User(StringUtils.isNotBlank(userDTO.getUid()) ? userDTO.getUid() : UUID.randomUUID().toString(),
				userDTO.getUsername(), userDTO.getPassword(), userDTO.getStatus());
		return userDao.save(user);
	}

	@Override
	public User updateUser(UserDTO userDTO) {
		User user = userDao.findById(userDTO.getUid()).get();
		if ("1".equals(user.getUid())&&!userDTO.getUsername().equals("admin")) {
			throw new BussinessException(BizExceptionEnum.NOT_UPDATE_ADMIN);
		}
		if (StringUtils.isNotBlank(userDTO.getUsername())) {
			user.setUsername(userDTO.getUsername());
		}
		if (StringUtils.isNotBlank(userDTO.getPassword())) {
			user.setPassword(userDTO.getPassword());
		}
		return userDao.save(user);
	}

	@Override
	public User updateUserStatus(String uid) {
		User user = userDao.findById(uid).get();
		if ("1".equals(user.getUid())) {
			throw new BussinessException(BizExceptionEnum.NOT_UPDATE_ADMIN);
		}
		user.setStatus(user.getStatus() == 1 ? 0 : 1);
		return userDao.save(user);
	}

	@Override
	public User resetUserPassword(String uid, String password) {
		User user = userDao.findById(uid).get();
		if ("1".equals(user.getUid())) {
			throw new BussinessException(BizExceptionEnum.NOT_UPDATE_ADMIN);
		}
		if (StringUtils.isBlank(password)) {
			password = "000000";
		}
		user.setPassword(password);
		return userDao.save(user);
	}

	@Override
	public void removeUser(String uid) {
		if ("1".equals(uid)) {
			throw new BussinessException(BizExceptionEnum.NOT_UPDATE_ADMIN);
		} else {
			userRoleDao.deleteByUid(uid);
			userDao.deleteById(uid);

		}

	}

}

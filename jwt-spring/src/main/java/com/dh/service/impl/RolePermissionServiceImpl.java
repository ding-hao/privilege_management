package com.dh.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dh.core.common.enums.BizExceptionEnum;
import com.dh.core.common.exception.BussinessException;
import com.dh.core.common.utils.CollectionUtil;
import com.dh.dao.PermissionDao;
import com.dh.dao.RolePermissionDao;
import com.dh.entity.Permission;
import com.dh.entity.RolePermission;
import com.dh.service.RolePermissionService;


/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:39:40 
 * @Description 类说明 :
 */
@Service
public class RolePermissionServiceImpl implements RolePermissionService {

	@Autowired
	private RolePermissionDao rolePermissionDao;
	
	@Autowired
	private PermissionDao permissionDao;
	
	@Override
	public List<Permission> findByRid(String rid) {
		List<Permission> permissionList = new ArrayList<>();
		List<RolePermission> rolePermissionList = rolePermissionDao.findByRid(rid);
		if(!CollectionUtils.isEmpty(rolePermissionList)) {
			for (RolePermission rolePermission : rolePermissionList) {
				Optional<Permission> findById = permissionDao.findById(rolePermission.getPid());
				if(findById.isPresent()) {
					permissionList.add(findById.get());
				}
			}
			return permissionList;
		}
		return null;
	}

	@Override
	public List<RolePermission> forRoleAddPermission(String rid, String pids) {
		List<RolePermission> rolePermissions = rolePermissionDao.findByRid(rid);
		List<String> pidList = Arrays.asList(pids.split(","));
		RolePermission rolePermission = null;
		if(!rolePermissions.isEmpty()) {
			Set<String> pidSet = rolePermissions.stream().map(RolePermission::getPid).collect(Collectors.toSet());
			Collection<String> removeExist = CollectionUtil.removeExist(pidSet, pidList);
			for (String pid : removeExist) {
				if(StringUtils.isNotBlank(pid)) {
					rolePermission = new RolePermission(UUID.randomUUID().toString(),rid,pid);
					RolePermission saveAndFlush = rolePermissionDao.saveAndFlush(rolePermission);
					// 报存成功
					if(saveAndFlush != null) {
						rolePermissions.add(saveAndFlush);
					}
				}
			}
		}else {
			for (String pid : pidList) {
				if(StringUtils.isNotBlank(pid)) {
					rolePermission = new RolePermission(UUID.randomUUID().toString(),rid,pid);
					RolePermission saveAndFlush = rolePermissionDao.saveAndFlush(rolePermission);
					// 报存成功
					if(saveAndFlush != null) {
						rolePermissions.add(saveAndFlush);
					} 
				}
			}
		}
		return rolePermissions;
	}

	@Override
	public List<RolePermission> forRoleUpdatePermission(String rid, String pids) {
		// 传递的角色id不能为空
		if(StringUtils.isNotBlank(rid)) {
			List<RolePermission> findByRid = rolePermissionDao.findByRid(rid);
			// 修改前的角色权限
			if(!CollectionUtils.isEmpty(findByRid)) {
				rolePermissionDao.deleteByRid(rid);
				List<String> pidList = Arrays.asList(pids.split(","));
				RolePermission rolePermission  = null;
				for (String pid : pidList) {
					if(StringUtils.isNotBlank(pid)) {
						rolePermission = new RolePermission(UUID.randomUUID().toString(),rid,pid);
						rolePermissionDao.save(rolePermission);
					}
				}
				// 角色不存在，去添加角色权限
			}else {
				throw new BussinessException(BizExceptionEnum.ROLE_NOT_PRESENT);
			}
			return rolePermissionDao.findByRid(rid);
		}
		return null;
	}

	@Override
	public Page<RolePermission> findRolePermissionList(String option, Integer page, Integer pagesize, String sort,
			String order) {
		RolePermission rolePermission = new RolePermission();
		if(StringUtils.isNotBlank(option)) {
			rolePermission.setRid(option);
		}
		
		Example<RolePermission> example = Example.of(rolePermission);
		
		PageRequest pageable = null;
		if(StringUtils.isNotBlank(sort)) {
			Direction direction = Direction.ASC.toString().equals(order) ? Direction.ASC : Direction.DESC;  
			Sort Sort = new Sort(direction, sort);
			pageable = PageRequest.of(page, pagesize, Sort);
		}else {
			pageable = PageRequest.of(page, pagesize);
		}
		
		return rolePermissionDao.findAll(example, pageable);
	}

}

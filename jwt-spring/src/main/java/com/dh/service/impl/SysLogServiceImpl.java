package com.dh.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.convert.QueryByExamplePredicateBuilder;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.dh.dao.SysLogDao;
import com.dh.dto.SysLogDTO;
import com.dh.entity.SysLog;
import com.dh.service.SysLogService;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年12月17日 下午2:47:33
 * @Description 类说明 :
 */
@Service
public class SysLogServiceImpl implements SysLogService {

	@Autowired
	private SysLogDao sysLogDao;

	@Override
	public SysLog addSysLog(SysLog sysLog) {
		return sysLogDao.save(sysLog);
	}

	@Override
	public Page<SysLog> findSysLogList(SysLogDTO sysLogDTO) {
		SysLog sysLog = new SysLog();
		if (StringUtils.isNotBlank(sysLogDTO.getActionName())) {
			sysLog.setActionName(sysLogDTO.getActionName());
		}
		if (StringUtils.isNotBlank(sysLogDTO.getIp())) {
			sysLog.setIp(sysLogDTO.getIp());
		}
		if (StringUtils.isNotBlank(sysLogDTO.getUri())) {
			sysLog.setUri(sysLogDTO.getUri());
		}
		if (StringUtils.isNotBlank(sysLogDTO.getUsername())) {
			sysLog.setUsername(sysLogDTO.getUsername());
		}

		// = 条件
		Example<SysLog> example = Example.of(sysLog);
		// 排序
		Sort Sort = new Sort(Direction.DESC, "createDate");
		PageRequest pageable = PageRequest.of(sysLogDTO.getPage() - 1, sysLogDTO.getPagesize(), Sort);

		// 时间段
		Specification<SysLog> spec = new Specification<SysLog>() {

			private static final long serialVersionUID = 1129220512158798136L;

			@Override
			public Predicate toPredicate(Root<SysLog> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {

				List<Predicate> predicates = new ArrayList<>();
				// 通过Example创建Predicate
				Predicate predicate = QueryByExamplePredicateBuilder.getPredicate(root, criteriaBuilder, example);
				predicates.add(predicate);
//				// 上面代码起始时间必须存在
//				if (sysLogDTO.getStartTime() != null) {
//					final Date startTime = sysLogDTO.getStartTime();
//					if (sysLogDTO.getEndTime() == null) {
//						final Date endTime = new Date();
//						predicates.add(
//								criteriaBuilder.between(root.get("createDate").as(Date.class), startTime, endTime));
//					} else {
//						final Date endTime = sysLogDTO.getEndTime();
//						predicates.add(
//								criteriaBuilder.between(root.get("createDate").as(Date.class), startTime, endTime));
//					}
//				} 
				// 将上面代码优化为: 起始时间,结束时间有一个就可以
				if (sysLogDTO.getStartTime() != null) {
					final Date startTime = sysLogDTO.getStartTime();
					predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("createDate").as(Date.class),startTime));
				}
				if(sysLogDTO.getEndTime() != null) {
					final Date endTime = sysLogDTO.getEndTime();
					predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("createDate").as(Date.class), endTime));
				}

				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};

		return sysLogDao.findAll(spec, pageable);
	}

}

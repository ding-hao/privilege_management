package com.dh.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.dh.dto.PermissionDTO;
import com.dh.entity.Permission;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月6日 上午9:35:52 
 * @Description 类说明 :
 */
public interface PermissionService {

	/**
	 * 
	 * @Title: findPermissionList
	 * @author  
	 * @Description: 按权限名称查询权限
	 * @param option
	 * @param page
	 * @param pagesize
	 * @param sort
	 * @param order
	 * @return
	 * @return Page<Permission>
	 */
	Page<Permission> findPermissionList(String option, Integer page, Integer pagesize, String sort, String order);

	/**
	 * 
	 * @Title: addPermission
	 * @author  
	 * @Description: 添加权限菜单
	 * @param permissionDTO
	 * @return
	 * @return Permission
	 */
	Permission addPermission(PermissionDTO permissionDTO);

	/**
	 * 
	 * @Title: updatePermission
	 * @author  
	 * @Description: 修改权限菜单
	 * @param permissionDTO
	 * @return
	 * @return Permission
	 */
	Permission updatePermission(PermissionDTO permissionDTO);

	/**
	 * 
	 * @Title: findAllPermissionList
	 * @author  
	 * @Description: 查询所有菜单权限
	 * @return
	 * @return List<Permission>
	 */
	List<Permission> findAllPermissionList();

	
	/**
	 * 
	 * @Title: removePermission
	 * @author  
	 * @Description: 根据pid删除菜单权限
	 * @param pid
	 * @return void
	 */
	void removePermission(String pid);

}

package com.dh.service;

import org.springframework.data.domain.Page;

import com.dh.dto.RoleDTO;
import com.dh.entity.Role;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:23:08 
 * @Description 类说明 :
 */
public interface RoleService {

	
	/**
	 * 
	 * @Title: findRoleList
	 * @author  
	 * @Description: 按条件查询角色
	 * @param option
	 * @param page
	 * @param pagesize
	 * @param sort
	 * @param order
	 * @return
	 * @return Page<Role>
	 */
	Page<Role> findRoleList(String option, Integer page, Integer pagesize, String sort, String order);

	/**
	 * 
	 * @Title: addRole
	 * @author  
	 * @Description: 添加角色
	 * @param roleDTO
	 * @return
	 * @return Role
	 */
	Role addRole(RoleDTO roleDTO);

	/**
	 * 
	 * @Title: updateRole
	 * @author  
	 * @Description: 修改角色
	 * @param roleDTO
	 * @return
	 * @return Role
	 */
	Role updateRole(RoleDTO roleDTO);


}

package com.dh.service;

import org.springframework.data.domain.Page;

import com.dh.dto.SysLogDTO;
import com.dh.entity.SysLog;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月17日 下午2:46:45 
 * @Description 类说明 :
 */
public interface SysLogService {

	/**
	 * 
	 * @Title: addSysLog
	 * @author  
	 * @Description: 添加日志
	 * @param sysLog
	 * @return void
	 */
	
	SysLog addSysLog(SysLog sysLog);

	/**
	 * 
	 * @Title: findSysLogList
	 * @author  
	 * @Description: 条件查询日志
	 * @param sysLogDTO
	 * @return
	 * @return List<SysLog>
	 */
	Page<SysLog> findSysLogList(SysLogDTO sysLogDTO);

}

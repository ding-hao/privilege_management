//package com.dh.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.dh.core.common.dto.PageParamDTO;
//import com.dh.entity.UserRole;
//import com.dh.service.UserRoleService;
//
///** 
// * @author 作者 dinghao
// * @version 创建时间：2018年12月5日 下午5:55:55 
// * @Description 类说明 :
// */
//@RestController
//@RequestMapping("/userRole")
//public class UserRoleController {
//
//	@Autowired
//	private UserRoleService userRoleService;
//	
//	@PostMapping("/add/{uid}")
//	public Object forUserToAddRole(@PathVariable("uid") String uid,@RequestParam("rids") String rids) {
//		List<UserRole> userRoles = userRoleService.forUserToAddRole(uid,rids);
//		return userRoles;
//	}
//	
//	@PostMapping("/update/{uid}")
//	public Object forUserToUpdateRole(@PathVariable("uid") String uid,@RequestParam("rids") String rids) {
//		List<UserRole> userRoles = userRoleService.forUserToUpdateRole(uid,rids);
//		return userRoles;
//	}
//	
//	@PostMapping("/list")
//	public Object findUserRoleList(@RequestBody PageParamDTO pageParamDTO) {
//		Page<UserRole> userRoles = userRoleService.findUserRoleList(pageParamDTO.getOption(), pageParamDTO.getPage(),
//				pageParamDTO.getPagesize(), pageParamDTO.getSort(), pageParamDTO.getOrder());
//		return ResponseEntity.ok(userRoles);
//	}
//	
//	@GetMapping("/get/{uid}")
//	public Object findByUid(@PathVariable("uid") String uid) {
//		return userRoleService.findByUid(uid);
//	}
//}

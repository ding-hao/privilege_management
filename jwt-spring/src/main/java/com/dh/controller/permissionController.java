package com.dh.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dh.core.common.dto.PageParamDTO;
import com.dh.core.support.Log.annotation.SysLogs;
import com.dh.dto.PermissionDTO;
import com.dh.entity.Permission;
import com.dh.service.PermissionService;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年12月5日 下午5:35:06
 * @Description 类说明 :
 */
@RestController
@RequestMapping("/permission")
public class permissionController {

	@Autowired
	private PermissionService permissionService;

	@SysLogs("查询所有菜单")
	@PostMapping("/all")
	public Object findAllPermissionList() {
		List<Permission> permissions = permissionService.findAllPermissionList();
		return ResponseEntity.ok(permissions);
	}
	
	@SysLogs("查询所有菜单(分页)")
	@PostMapping("/list")
	public Object findPermissionList(@RequestBody PageParamDTO pageParamDTO) {
		Page<Permission> permissions = permissionService.findPermissionList(pageParamDTO.getOption(), pageParamDTO.getPage(),
				pageParamDTO.getPagesize(), pageParamDTO.getSort(), pageParamDTO.getOrder());
		return ResponseEntity.ok(permissions);
	}
	
	@SysLogs("添加菜单")
	@PostMapping("/add")
	public Object addPermission(@RequestBody PermissionDTO permissionDTO) {
		Permission addPermission = permissionService.addPermission(permissionDTO);
		if(addPermission!= null) {
			return "添加权限菜单成功";
		}
		return "添加权限菜单失败";
	}
	
	@SysLogs("修改菜单")
	@PostMapping("/update")
	public Object updatePermission(@RequestBody PermissionDTO permissionDTO) {
		Permission updatePermission = permissionService.updatePermission(permissionDTO);
		if(updatePermission!= null) {
			return "修改权限菜单成功";
		}
		return "修改权限菜单失败";
	}
	
	@SysLogs("刪除菜单")
	@GetMapping("/remove")
	public Object removePermission(@RequestParam("pid") String pid) {
		permissionService.removePermission(pid);
		return ResponseEntity.ok("删除菜单权限成功!");
	}

}

package com.dh.controller;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dh.core.auth.annotation.jwtAuth;
import com.dh.core.common.controller.BaseController;
import com.dh.core.common.utils.BeanUtil;
import com.dh.core.common.warpper.UserWarpper;
import com.dh.entity.User;

/**
 * 
* @ClassName: ExampleController 
* @Description: 常规控制器
* @author dinghao
* @date 2018年12月4日 下午4:04:38 
*
 */
@RestController
@RequestMapping("/hello")
public class ExampleController extends BaseController{


	@PostMapping({"/jwtAuth"})
    @jwtAuth
    public ResponseEntity<String> hello(@RequestBody User user) {
        System.out.println(user.getPassword()+user.getUsername());
        
        return ResponseEntity.ok("请求成功!");
    }
    
	@PostMapping({"/lala"})
    public ResponseEntity<String> lala(@RequestBody User user) {
        System.out.println(user.getUsername());
        return ResponseEntity.ok("请求成功!");
    }
    
    @GetMapping("/user")
    public Object list() throws Exception {
        User user = new User();
        user.setUsername("张三");
        Map<String, Object> objectToMap = BeanUtil.objectToMap(user);
        return super.warpObject(new UserWarpper(objectToMap));
    }
}

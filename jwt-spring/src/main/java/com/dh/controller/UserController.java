package com.dh.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dh.core.common.controller.BaseController;
import com.dh.core.common.dto.PageParamDTO;
import com.dh.core.common.utils.BeanUtil;
import com.dh.core.support.Log.annotation.SysLogs;
import com.dh.dto.UserDTO;
import com.dh.entity.Role;
import com.dh.entity.User;
import com.dh.service.UserRoleService;
import com.dh.service.UserService;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年12月5日 下午3:09:56
 * @Description 类说明 :
 */

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Deprecated
	@PostMapping("/list")
	public Object findUserList(@RequestBody PageParamDTO pageParamDTO) {
		Page<User> users = userService.findUserList(pageParamDTO.getOption(), pageParamDTO.getPage(),
				pageParamDTO.getPagesize(), pageParamDTO.getSort(), pageParamDTO.getOrder());
		return ResponseEntity.ok(users);
	}

	@SysLogs("查询用户")
	@PostMapping("/role/list")
	public Object findUserAndRoleList(@RequestBody PageParamDTO pageParamDTO) {
		Page<User> users = userService.findUserList(pageParamDTO.getOption(), pageParamDTO.getPage(),
				pageParamDTO.getPagesize(), pageParamDTO.getSort(), pageParamDTO.getOrder());
		List<Map<String, Object>> listMap = new ArrayList<>();
		Map<String, Object> map = null;
		List<Role> roles = null;
		for (User user : users.getContent()) {
			map = BeanUtil.objectToMap(user);
			roles = userRoleService.findByUid(user.getUid());
			map.put("roles", roles);
			listMap.add(map);
		}
		int number = users.getNumber();
		int size = users.getSize();
		long totalElements = users.getTotalElements();
		Map<String,Object> resultMap = new HashMap<>();
		resultMap.put("content", listMap);
		resultMap.put("number", number);
		resultMap.put("size", size);
		resultMap.put("totalElements", totalElements);
		return ResponseEntity.ok(resultMap);
	}
	
	/**
	 * 
	 * @Title: saveUser
	 * @author  
	 * @Description: 包括user对象具有的角色属性
	 * @param userDTO
	 * @return
	 * @return Object
	 */
	@SysLogs("添加用户")
	@PostMapping("/add")
	public Object addUser(@RequestBody UserDTO userDTO) {
		User addUser = userService.addUser(userDTO);
		userRoleService.forUserToAddRole(addUser.getUid(), userDTO.getRids());
		return ResponseEntity.ok("用户添加成功!");
	}

	/**
	 * 
	 * @Title: updateUser
	 * @author  
	 * @Description: 包括user对象具有的角色属性
	 * @param userDTO
	 * @return
	 * @return Object
	 */
	@SysLogs("修改用户信息")
	@PostMapping("/update")
	public Object updateUser(@RequestBody UserDTO userDTO) {
		User updateUser = userService.updateUser(userDTO);
		userRoleService.forUserToUpdateRole(updateUser.getUid(), userDTO.getRids());
		return ResponseEntity.ok("用户修改成功!");
	}

	@SysLogs("修改用户状态")
	@GetMapping("/updateStatus")
	public Object updateUserStatus(@RequestParam("uid") String uid) {
		userService.updateUserStatus(uid);
		return ResponseEntity.ok("修改用户状态成功!");
	
	}

	@GetMapping("/resetPassword")
	@SysLogs("重置用户密码")
	public Object resetUserPassword(@RequestParam("uid") String uid, @RequestParam("password") String password) {
		userService.resetUserPassword(uid, password);
		return ResponseEntity.ok("重置用户密码成功!");
	}
	
	@SysLogs("")
	@GetMapping("/remove")
	public Object removeUser(@RequestParam("uid") String uid) {
		userService.removeUser(uid);
		return ResponseEntity.ok("删除用户成功!");
	}
}

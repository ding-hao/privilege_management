package com.dh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dh.dto.SysLogDTO;
import com.dh.entity.SysLog;
import com.dh.service.SysLogService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月17日 下午2:48:51 
 * @Description 类说明 :
 */

@RestController
@RequestMapping("/sysLog")
public class SysLogController {

	@Autowired
	private SysLogService sysLogService;
	
	@PostMapping("/list")
	public Object findSysLogList(@RequestBody SysLogDTO sysLogDTO) {
		Page<SysLog> sysLogs = sysLogService.findSysLogList(sysLogDTO);
		return ResponseEntity.ok(sysLogs);
	}
	
}

package com.dh.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dh.core.common.dto.PageParamDTO;
import com.dh.core.support.Log.annotation.SysLogs;
import com.dh.dto.RoleDTO;
import com.dh.entity.Permission;
import com.dh.entity.Role;
import com.dh.service.RolePermissionService;
import com.dh.service.RoleService;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年12月5日 下午5:34:29
 * @Description 类说明 :
 */
@RestController
@RequestMapping("/role")
public class RoleController {

	@Autowired
	private RoleService roleService;

	@Autowired
	private RolePermissionService rolePermissionService;

	@PostMapping("/list")
	@SysLogs("查询所有角色")
	public Object findRoleList(@RequestBody PageParamDTO pageParamDTO) {
		Page<Role> roles = roleService.findRoleList(pageParamDTO.getOption(), pageParamDTO.getPage(),
				pageParamDTO.getPagesize(), pageParamDTO.getSort(), pageParamDTO.getOrder());
		return ResponseEntity.ok(roles);
	}

	@SysLogs("查询指定角色的菜单权限")
	@GetMapping("/permission")
	public Object findPermissionByRid(@RequestParam("rid") String rid) {
		List<Permission> permissions = rolePermissionService.findByRid(rid);
		return permissions;
	}

	@SysLogs("添加角色")
	@PostMapping("/add")
	public Object addRole(@RequestBody RoleDTO roleDTO) {
		Role addRole = roleService.addRole(roleDTO);
		rolePermissionService.forRoleAddPermission(addRole.getRid(), roleDTO.getPids());
		return ResponseEntity.ok("角色添加成功!");
	}

	@SysLogs("修改角色")
	@PostMapping("/update")
	public Object updateRole(@RequestBody RoleDTO roleDTO) {
		Role updateRole = roleService.updateRole(roleDTO);
		rolePermissionService.forRoleUpdatePermission(updateRole.getRid(), roleDTO.getPids());
		return ResponseEntity.ok("角色修改成功!");
	}
}

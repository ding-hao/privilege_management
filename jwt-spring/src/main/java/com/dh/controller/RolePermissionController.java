//package com.dh.controller;
//
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.dh.core.common.dto.PageParamDTO;
//import com.dh.entity.Permission;
//import com.dh.entity.RolePermission;
//import com.dh.service.RolePermissionService;
//
///** 
// * @author 作者 dinghao
// * @version 创建时间：2018年12月5日 下午5:57:03 
// * @Description 类说明 :
// */
//@RestController
//@RequestMapping("/rolePermission")
//public class RolePermissionController {
//
//	@Autowired
//	private RolePermissionService rolePermissionService;
//
//	@GetMapping("/{rid}")
//	public Object findPermissionByRid(@PathVariable("rid") String rid) {
//		List<Permission> permissions = rolePermissionService.findByRid(rid);
//		return permissions;
//	}
//	
//	@PostMapping("/add/{rid}")
//	public Object forRoleAddPermission(@PathVariable("rid") String rid,@RequestParam("pids") String pids) {
//		List<RolePermission> rolePermissions =  rolePermissionService.forRoleAddPermission(rid,pids);
//		return rolePermissions;
//	}
//	
//	@PostMapping("/update/{rid}")
//	public Object forRoleUpdatePermission(@PathVariable("rid") String rid,@RequestParam("pids") String pids) {
//		List<RolePermission> rolePermissions =  rolePermissionService.forRoleUpdatePermission(rid,pids);
//		return rolePermissions;
//	}
//	
//	@PostMapping("/list")
//	public Object findRolePermissionList(@RequestBody PageParamDTO pageParamDTO) {
//		Page<RolePermission> rolePermissions = rolePermissionService.findRolePermissionList(pageParamDTO.getOption(), pageParamDTO.getPage(),
//				pageParamDTO.getPagesize(), pageParamDTO.getSort(), pageParamDTO.getOrder());
//		return ResponseEntity.ok(rolePermissions);
//	}
//	
//	@GetMapping("/role/list")
//	public Object findPermissionByRidList(@RequestParam("rid") String rid) {
//		 List<Permission> permissions = rolePermissionService.findByRid(rid);
//		return ResponseEntity.ok(permissions);
//	}
//	
//}

package com.dh.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.entity.SysLog;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年12月17日 下午2:44:19
 * @Description 类说明 :
 */
public interface SysLogDao extends JpaRepository<SysLog, String>, JpaSpecificationExecutor<SysLog> {

}

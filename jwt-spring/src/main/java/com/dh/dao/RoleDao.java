package com.dh.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.entity.Role;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午1:45:31 
 * @Description 类说明 :
 */
public interface RoleDao extends JpaRepository<Role, String>, JpaSpecificationExecutor<Role> {

}

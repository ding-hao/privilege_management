package com.dh.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.entity.User;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午1:44:26 
 * @Description 类说明 :
 */
public interface UserDao extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

	List<User> findByUsernameAndStatus(String username,Integer status);

	List<User> findByUsername(String username);

}

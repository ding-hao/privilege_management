package com.dh.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;

import com.dh.entity.UserRole;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:13:14 
 * @Description 类说明 :
 */
public interface UserRoleDao extends JpaRepository<UserRole, String>, JpaSpecificationExecutor<UserRole> {

	/**
	 * 
	 * @Title: findByUid
	 * @author  
	 * @Description: 根据用户id查询用户角色
	 * @param uid
	 * @return
	 * @return List<UserRole>
	 */
	List<UserRole> findByUid(String uid);

	/**
	 * 
	 * @Title: deleteByUid
	 * @author  
	 * @Description: 根据用户id删除用户角色
	 * @param uid
	 * @return void
	 */
	@Modifying
	@Transactional
	void deleteByUid(String uid);

}

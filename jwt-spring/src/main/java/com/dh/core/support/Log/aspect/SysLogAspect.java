package com.dh.core.support.Log.aspect;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dh.core.auth.utils.JwtTokenUtil;
import com.dh.core.support.Log.annotation.SysLogs;
import com.dh.entity.SysLog;
import com.dh.service.SysLogService;


/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月17日 下午2:34:57 
 * @Description 类说明 : 日志AOP
 */
@Aspect
@Component
public class SysLogAspect {

    private final SysLogService sysLogService;
    
    @Autowired
	private JwtTokenUtil jwtTokenUtil;

    @Autowired
    public SysLogAspect(SysLogService sysLogService) {
        this.sysLogService = sysLogService;
    }

    @Pointcut("@annotation(com.dh.core.support.Log.annotation.SysLogs)")
    public void log(){}

    @AfterReturning("log()")
    public void after(JoinPoint joinPoint){
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        
       
        SysLog sysLog = new SysLog();
        sysLog.setId(UUID.randomUUID().toString());
        // 获取动作Action释义
        sysLog.setActionName(getMethodSysLogsAnnotationValue(joinPoint));
        // 获取IP
        sysLog.setIp(request.getRemoteAddr());
        // 获取请求路径
        sysLog.setUri(request.getRequestURI());
        // 获取请求参数
        sysLog.setParams(StringUtils.join(joinPoint.getArgs(), ","));
		// 获取http请求方法
        sysLog.setHttpMethod(request.getMethod());
        // 获取请求的类方法
        sysLog.setClassMethod(joinPoint.getSignature().getDeclaringTypeName()
                + "." + joinPoint.getSignature().getName()+"()");
        // 获取操作用户
        String token = request.getHeader("Authorization").substring(7);;
        sysLog.setUsername(jwtTokenUtil.getUsernameFromToken(token));
        // 日志创建的时间
        sysLog.setCreateDate(new Date());
        sysLogService.addSysLog(sysLog);
    }

    private String getMethodSysLogsAnnotationValue(JoinPoint joinPoint){
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature)signature;
        Method method = methodSignature.getMethod();
        if(method.isAnnotationPresent(SysLogs.class)){
            //获取方法上注解中说明
            SysLogs sysLogs = method.getAnnotation(SysLogs.class);
            return sysLogs.value();
        }
        return "未知";
    }

	
}

package com.dh.core.support.Xxs;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.io.IOException;


public class XssFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(XssFilter.class);
	
    FilterConfig filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        LOGGER.debug("xssFilter initialized");
    }

    public void destroy() {
        this.filterConfig = null;
        LOGGER.debug("xssFilter destroy");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        chain.doFilter(new XssHttpServletRequestWrapper(
                (HttpServletRequest) request), response);
    }

}
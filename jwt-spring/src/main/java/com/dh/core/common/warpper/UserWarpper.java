package com.dh.core.common.warpper;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.dh.entity.Role;
import com.dh.service.UserRoleService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 下午5:35:29 
 * @Description 类说明 :
 */
public class UserWarpper extends BaseControllerWarpper{
	
	@Autowired
	private UserRoleService userRoleService;
	
	public UserWarpper(Object list) {
		super(list);
	}


	@Override
	protected void warpTheMap(Map<String, Object> map) {
		String uid = (String)map.get("uid");
		List<Role> findByUid = userRoleService.findByUid(uid);
		map.put("roles", findByUid);
	}

}

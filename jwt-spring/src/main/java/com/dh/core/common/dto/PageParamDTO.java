package com.dh.core.common.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月5日 下午3:51:08 
 * @Description 类说明 : 分页传参dto
 */
@ApiModel("分页条件查询对象")
public class PageParamDTO implements Serializable{
	
	private static final long serialVersionUID = -2050373846003970146L;
	
	@ApiModelProperty(value = "查询条件")
	private String option;
	
	@ApiModelProperty(value = "页码")
	private Integer page;
	
	@ApiModelProperty(value = "每页数量")
	private Integer pagesize;
	
	@ApiModelProperty(value = "排序字段")
	private String sort;
	
	@ApiModelProperty(value = "升/降序")
	private String order;
	
	public String getOption() {
		return option;
	}
	public void setOption(String option) {
		this.option = option;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getPagesize() {
		return pagesize;
	}
	public void setPagesize(Integer pagesize) {
		this.pagesize = pagesize;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	@Override
	public String toString() {
		return "PageParamDTO [option=" + option + ", page=" + page + ", pagesize=" + pagesize + ", sort=" + sort
				+ ", order=" + order + "]";
	}
}

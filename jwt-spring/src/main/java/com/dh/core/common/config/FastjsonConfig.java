package com.dh.core.common.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.serializer.ValueFilter;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.dh.core.auth.converter.WithSignMessageConverter;

@Configuration
public class FastjsonConfig extends WebMvcConfigurationSupport {

	// 日志
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private WithSignMessageConverter fastConverter;

	/**
	 * 利用fastjson替换掉jackson，且解决中文乱码问题
	 * 
	 * @param converters
	 */
	// 方法一：extends WebMvcConfigurerAdapter
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		super.configureMessageConverters(converters);
		// 核心代码

		/*
		 * 1、先定义一个convert转换消息的对象 2、添加fastjson的配置信息，比如是否要格式化返回的json数据； 3、在convert中添加配置信息
		 * 4、将convert添加到converters
		 */

		// 1、先定义一个convert转换消息的对象
		// FastJsonHttpMessageConverter fastConverter = new
		// FastJsonHttpMessageConverter();
		// 自定义convert转换消息的对象
		// WithSignMessageConverter fastConverter = new WithSignMessageConverter();

		// 2、添加fastjson的配置信息，比如是否要格式化返回的json数据；
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(
				// 是否需要格式化
				SerializerFeature.PrettyFormat);
		// 设置日期转换格式
		fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
		// 设置空值的处理方式
		ValueFilter valueFilter = new ValueFilter() {
			public Object process(Object o, String s, Object o1) {
				if (null == o1) {
					o1 = "";
				}
				return o1;
			}
		};
		fastJsonConfig.setSerializeFilters(valueFilter);
		
		// 附加：处理中文乱码（后期添加）
		List<MediaType> fastMedisTypes = new ArrayList<MediaType>();
		fastMedisTypes.add(MediaType.APPLICATION_JSON_UTF8);

		// 3、在convert中添加配置信息
		fastConverter.setSupportedMediaTypes(fastMedisTypes);
		fastConverter.setFastJsonConfig(fastJsonConfig);
		// 4、将convert添加到converters
		converters.add(fastConverter);
		logger.info("利用fastjson替换掉jackson，且解决中文乱码问题");
	}
	
	
	/**
     * 发现如果继承了WebMvcConfigurationSupport，则在yml中配置的相关内容会失效。 需要重新指定静态资源
     * 增加swagger静态资源释放的配置
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**").addResourceLocations(
                "classpath:/static/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations(
                "classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations(
                "classpath:/META-INF/resources/webjars/");
        super.addResourceHandlers(registry);
    }
	
//	  /**
//     * 配置servlet处理
//     */
//    @Override
//    public void configureDefaultServletHandling(
//            DefaultServletHandlerConfigurer configurer) {
//        configurer.enable();
//    }
}

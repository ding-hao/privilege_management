package com.dh.core.common.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;

/**
 * 
 * @author dinghao
 *
 */
@Configuration
@EnableConfigurationProperties(DruidPropertityConfig.class)
public class DruidConfig {

	// 日志
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private DruidPropertityConfig propertityConfig;

	/**
	 * 配置数据库的基本链接信息
	 * 
	 * druid属性配置 Springboot 默认使用org.apache.tomcat.jdbc.pool.DataSource数据源，默认配置如下：
	 * Springboot默认支持4种数据源类型，定义在
	 * org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
	 * 中，分别是： org.apache.tomcat.jdbc.pool.DataSource
	 * com.zaxxer.hikari.HikariDataSource org.apache.commons.dbcp.BasicDataSource
	 * org.apache.commons.dbcp2.BasicDataSource 对于这4种数据源，当 classpath
	 * 下有相应的类存在时，Springboot 会通过自动配置为其生成DataSource Bean，DataSource
	 * Bean默认只会生成一个，四种数据源类型的生效先后顺序如下：Tomcat--> Hikari --> Dbcp --> Dbcp2 。
	 * 
	 * @param propertityConfig
	 * @return
	 */
	@Bean(name = "dataSource")
	@Primary
	public DataSource druidDataSource() {

		logger.info("dataSource propertityConfig:{}", propertityConfig);

		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setUrl(propertityConfig.getUrl());
		dataSource.setUsername(propertityConfig.getUsername());
		dataSource.setPassword(propertityConfig.getPassword());
		dataSource.setDriverClassName(propertityConfig.getDriverClassName());
		// configuration
		dataSource.setInitialSize(propertityConfig.getInitialSize());
		dataSource.setMinIdle(propertityConfig.getMinIdle());
		dataSource.setMaxActive(propertityConfig.getMaxActive());
		dataSource.setMaxWait(propertityConfig.getMaxWait());
		dataSource.setTimeBetweenEvictionRunsMillis(propertityConfig.getTimeBetweenEvictionRunsMillis());
		dataSource.setMinEvictableIdleTimeMillis(propertityConfig.getMinEvictableIdleTimeMillis());
		dataSource.setTestWhileIdle(propertityConfig.isTestWhileIdle());
		dataSource.setTestOnBorrow(propertityConfig.isTestOnBorrow());
		dataSource.setTestOnReturn(propertityConfig.isTestOnReturn());
		dataSource.setPoolPreparedStatements(propertityConfig.isPoolPreparedStatements());
		dataSource.setMaxPoolPreparedStatementPerConnectionSize(
				propertityConfig.getMaxPoolPreparedStatementPerConnectionSize());
		return dataSource;
	}

	/**
	 * 注册一个druidStatViewServlet
	 * 
	 * @return
	 */
	@Bean
	public ServletRegistrationBean<StatViewServlet> druidStatViewServlet() {
		ServletRegistrationBean<StatViewServlet> servletRegistrationBean = new ServletRegistrationBean<StatViewServlet>(new StatViewServlet(),
				"/druid/*");
		// 白名单
		servletRegistrationBean.addInitParameter("allow", "127.0.0.1");
		/*
		 * IP黑名单 (存在共同时，deny优先于allow) : 如果满足deny的即提示:Sorry, you are not permitted to
		 * view this page.
		 * 
		 * 不允许访问的IP
		 */
//		servletRegistrationBean.addInitParameter("deny", "192.168.1.100");
		// 登录查看信息的账号密码.
		servletRegistrationBean.addInitParameter("loginUsername", "admin");
		servletRegistrationBean.addInitParameter("loginPassword", "123456");
		// 是否能够重置数据.
		servletRegistrationBean.addInitParameter("resetEnable", "false");
		return servletRegistrationBean;
	}

	/**
	 * 注册一个druidStatFilter
	 * 
	 * @return
	 */
	@Bean
	public FilterRegistrationBean<WebStatFilter> druidStatFilter() {
		FilterRegistrationBean<WebStatFilter> filterRegistrationBean = new FilterRegistrationBean<WebStatFilter>(new WebStatFilter());
		// 添加过滤规则.
		filterRegistrationBean.addUrlPatterns("/*");
		// 添加不需要忽略的格式信息.
		filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		return filterRegistrationBean;
	}

	@Bean
	PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

}

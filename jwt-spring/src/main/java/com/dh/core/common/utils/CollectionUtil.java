package com.dh.core.common.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月6日 上午11:24:47 
 * @Description 类说明 :
 */
public class CollectionUtil {

	public static <T> Collection<T> removeExist(Collection<T> collection1 ,Collection<T> collection2){
		Collection<T> collection3 = new HashSet<T>();
		collection3.addAll(collection2);
		for (T str2 : collection2) {
			for (T str1 : collection1) {
				if(str1.equals(str2)) {
					collection3.remove(str2);
				}
			}
		}
		return collection3;
	}
	
	public static void main(String[] args) {
//		Set<String> set = new HashSet<>();
		Set<Integer> set = new HashSet<>();
//		set.add("abc");
//		set.add("bcd");
//		set.add("cde");
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(4);
		set.add(5);
//		List<String> list = new ArrayList<>();
		List<Integer> list = new ArrayList<>();
//		list.add("abc");
//		list.add("bcd");
//		list.add("def");
//		list.add("dinghao");
//		list.add("lixixi");
//		list.add("hahaha");
		
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(6);
		list.add(7);
		list.add(8);
//		Collection<String> removeExist = removeExist(set,list);
		Collection<Integer> removeExist = removeExist(set,list);
		System.out.println(removeExist);
	}
	
}

package com.dh.core.common.enums;

/**
 * 
 * @ClassName: BizExceptionEnum
 * @Description: 所有业务异常的枚举
 * @author dinghao
 * @date 2018年11月30日 上午10:10:51
 *
 */
public enum BizExceptionEnum {

	/**
	 * 其他
	 */
	WRITE_ERROR(500,"渲染界面错误"),
	
	/**
	 * token异常
	 */
	TOKEN_EXPIRED(700, "token过期，请重新登录"), 
	
	/**
	 * token验证失败
	 */
	TOKEN_ERROR(701, "token验证失败"),
	
	/**
	 * 用户未登录
	 */
	USER_NO_LOGIN(401, "用户未登录"),
	
	/**
	 * 用户已退出登录,或者token已失效
	 */
	USER_LOGOUT(402, "用户已退出登录,或者token已失效"),
	
	/**
	 * 用户已退出登录,或者token已失效
	 */
	USER_NO_PRESENT(403, "该用户不存在,修改用户或修改用户角色失败"),
	
	/**
	 * 签名异常
	 */
	SIGN_ERROR(702, "签名验证失败"),
	
	PARAMES_ERROR(703,"参数格式异常"),
	
	PARAMES_JSON_ERROR(704,"参数Json格式异常"),
	
	
	/**
	 * 用户没有权限
	 */
	NO_PERMISSION(300, "该用户没有权限"),
	
	/**
	 * 用户没有任何角色
	 */
	NO_ROLE(301, "该用户没有任何角色"),

	/**
	 * 角色不存在
	 */
	ROLE_NOT_PRESENT(302,"该角色不存在,修改角色或角色权限失败"),
	
	/**
	 * 其他
	 */
	AUTH_REQUEST_ERROR(400, "账号密码错误"), 
	
	NOT_PERMISSION(600,"权限菜单不存在"), 
	
	NOT_UPDATE_ADMIN(705,"不能修改超级管理员"),
	
	;

	BizExceptionEnum(int code, String message) {
		this.friendlyCode = code;
		this.friendlyMsg = message;
	}

	private int friendlyCode;

	private String friendlyMsg;

	public int getCode() {
		return friendlyCode;
	}

	public void setCode(int code) {
		this.friendlyCode = code;
	}

	public String getMessage() {
		return friendlyMsg;
	}

	public void setMessage(String message) {
		this.friendlyMsg = message;
	}

}

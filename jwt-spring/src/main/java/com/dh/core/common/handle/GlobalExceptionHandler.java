package com.dh.core.common.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.dh.core.auth.tip.ErrorTip;
import com.dh.core.common.enums.BizExceptionEnum;
import com.dh.core.common.exception.BussinessException;

import io.jsonwebtoken.JwtException;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 下午5:20:35 
 * @Description 类说明 :
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 拦截jwt相关异常
     */
    
    // 拦截指定异常
    @ExceptionHandler(JwtException.class)
    // 响应给客户端的状态
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorTip jwtException(JwtException e) {
    	log.error("Jwt异常:",e);
        return new ErrorTip(BizExceptionEnum.TOKEN_ERROR.getCode(), BizExceptionEnum.TOKEN_ERROR.getMessage());
    }
    
    @ExceptionHandler(BussinessException.class)
//    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorTip bussinessException(BussinessException e) {
    	log.error("业务异常:", e);
        return new ErrorTip(e.getCode(), e.getMessage());
    }
    
    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    public ErrorTip bussinessException(RuntimeException e) {
    	log.error("系统异常:", e);
        return new ErrorTip(500, e.getMessage());
    }
    
}

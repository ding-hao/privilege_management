package com.dh.core.common.controller;

import com.dh.core.common.warpper.BaseControllerWarpper;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 下午5:41:55 
 * @Description 类说明 :
 */
public class BaseController {

	/**
     * 包装一个list，让list增加额外属性
     */
    protected Object warpObject(BaseControllerWarpper warpper) {
        return warpper.warp();
    }
	
}

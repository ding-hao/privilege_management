package com.dh.core.auth.tip;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 上午10:43:32
 * @Description 类说明 : 错误提示类
 */
public class ErrorTip extends Tip{

	public ErrorTip(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}
}

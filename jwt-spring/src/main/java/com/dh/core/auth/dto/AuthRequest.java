package com.dh.core.auth.dto; 
/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 上午10:27:58 
 * @Description 类说明 : 登录时账户密码等信息
 */
public class AuthRequest {

    private String userName;
    private String password;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }
	
}

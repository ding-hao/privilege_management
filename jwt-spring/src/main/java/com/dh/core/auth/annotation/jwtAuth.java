package com.dh.core.auth.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月4日 下午1:43:53 
 * @Description 类说明 : 在请求的方法上加入该注解，在参数@RequestBody封装参数时传递的参数要是加密的json字符串
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface jwtAuth {

}

package com.dh.core.auth.dto;

/**
 * 
* @ClassName: BaseTransferEntity 
* @Description: 基础的传输bean 
* @author dinghao
* @date 2018年11月30日 上午10:15:28 
*
 */
public class BaseTransferEntity {

    private Object object;

    private String sign;

    public Object getObject() {
        return object;
    }

    public String getSign() {
        return sign;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}

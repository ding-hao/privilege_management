package com.dh.core.auth.tip; 
/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 上午10:44:10 
 * @Description 类说明 :返回给前台的提示（最终转化为json形式）
 */
public abstract class Tip {

    protected int code;
    protected String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

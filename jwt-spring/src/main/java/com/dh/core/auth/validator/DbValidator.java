package com.dh.core.auth.validator;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dh.core.auth.dto.AuthRequest;
import com.dh.dao.UserDao;
import com.dh.entity.User;

/**
 * 账号密码验证
 *
 * @author fengshuonan
 * @date 2017-08-23 12:34
 */
@Service
public class DbValidator implements IReqValidator {

	@Autowired
	private UserDao userDao;
	
    @Override
    public boolean validate(AuthRequest authRequest) {
    	List<User> users = userDao.findByUsernameAndStatus(authRequest.getUserName(),1);
        if (users != null && users.size() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

package com.dh.core.auth.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月6日 下午3:28:22 
 * @Description 类说明 : 系统内部管理token  tokenManagementUtil
 *		 * 1. 登入时添加进去
 *		 * 2. 登出时删除
 *		 * 解决jwt无法退出而使jwt失效的问题
 */
public class TokenManagementUtil {
	private static Map<String, String> tokenMap = new HashMap<>();
	
	
	public static boolean addToken(String username,String token) {
		String put = tokenMap.put(username, token);
		if(StringUtils.isNotBlank(put)){
			return true;
		}
		return false;
	}
	
	public static boolean removeToken(String username) {
		String remove = tokenMap.remove(username);
		if(StringUtils.isNotBlank(remove)){
			return true;
		}
		return false;
	}
	
	public static boolean tokenIsExist(String token) {
		return tokenMap.containsValue(token);
	}
}

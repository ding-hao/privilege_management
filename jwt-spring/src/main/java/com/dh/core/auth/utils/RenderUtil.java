package com.dh.core.auth.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.dh.core.common.enums.BizExceptionEnum;
import com.dh.core.common.exception.BussinessException;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 上午10:38:54
 * @Description 类说明 : 将响应的结果已json的形式写出
 */
public class RenderUtil {

	/**
	 * 渲染json对象
	 */
	public static void renderJson(HttpServletResponse response, Object jsonObject) {
		try {
			response.setCharacterEncoding("UTF-8");
			// 设置相应的类型为文本
			response.setContentType("text/plain; charset=UTF-8"); 
			PrintWriter writer = response.getWriter();
			writer.write(JSON.toJSONString(jsonObject));
		} catch (IOException e) {
			throw new BussinessException(BizExceptionEnum.WRITE_ERROR);
		}
	}

}

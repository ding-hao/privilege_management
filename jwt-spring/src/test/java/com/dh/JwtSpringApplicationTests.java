package com.dh;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.dh.core.auth.dto.BaseTransferEntity;
import com.dh.core.auth.utils.MD5Util;
import com.dh.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtSpringApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	/**
	 * 
	 * @Title: jiami
	 * @author  
	 * @Description: 请求参数加密后的信息,接口进行过处理传明文接受不了
	 * @return void
	 */
	@Test
	public void jiami() {
        String salt = "0dmcip";
        
        User user = new User();
        user.setUsername("李四");
        user.setPassword("123456");
        String md5 = MD5Util.encrypt(JSON.toJSONString(user) + salt);
        
        BaseTransferEntity baseTransferEntity = new BaseTransferEntity();
        baseTransferEntity.setObject(user);
        baseTransferEntity.setSign(md5);
        
        System.out.println(JSON.toJSON(baseTransferEntity));
        
        System.out.println("测试加密:"+MD5Util.encrypt("dinghao"));
	}

}

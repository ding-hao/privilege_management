# 权限管理

#### 项目介绍

权限管理框架

目录介绍:

spring-security ---------------- springboot整合security的demo例子

spring-shiro ------------------- springboot整合shiro的demo例子

spring-jwt --------------------- 服从jwt协议的demo例子

jwt-spring --------------------- 基于jwt协议的权限管理框架(自定义实现)

    jwt-spring
    |-----------controller ----------------------- 控制层
    |-----------core ----------------------------- 核心代码
            |-----------auth --------------------- jwt/权限相关
                    |----......---------
            |-----------common ------------------- 公共类/全局类
                    |----......---------
    |-----------dao ------------------------------ 数据层
    |-----------dto ------------------------------ 前后端传输对象
    |-----------entity --------------------------- 实体层
    |-----------service -------------------------- 业务层

#### 软件架构

软件架构说明

jwt-spring 是一个服从jwt协议简单的权限控制系统,该系统实现了动态控制用户的角色和菜单权限,支持通过@jwtAuth实现请求数据的加密传输,加有@jwtAuth的请求要求传输过来的数据必须是密文.系统也完美整合swagger2,可以通过swagger完整演示整个用户从创建开始到赋予角色\菜单权限,访问请求,退出等等所有过程.系统也解决了jwt无法让token随登出失效的问题.

该系统前端VUE工程: https://gitee.com/ding-hao/privilege_management_vue
#### 软件演示
所有的演示均在swagger上完成,前端VUE工程上有vue上的演示效果

访问swagger
![输入图片说明](https://images.gitee.com/uploads/images/2018/1207/141446_9e9a1f54_1636840.png "屏幕截图.png")
管理员登录
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/登录.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/登录.gif

创建用户
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/创建用户.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/创建用户.gif

创建角色
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/创建角色.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/创建角色.gif

添加菜单
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/查询菜单.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/查询菜单.gif

为用户添加角色
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/为用户添加角色.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/为用户添加角色.gif

为角色添加菜单权限
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/为角色添加权限.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/为角色添加权限.gif

登出
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/登出.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/登出.gif

张三登录
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/张三登录.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/张三登录.gif

张三访问
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/张三访问.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/张三访问.gif

张三访问加密接口
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/张三访问加密接口.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/张三访问加密接口.gif

修改用户角色
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/修改张三角色.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/修改张三角色.gif

修改角色菜单权限
![输入图片说明](https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/修改张三角色菜单权限.gif)

https://gitee.com/ding-hao/privilege_management/tree/master/jwt-spring/image/修改张三角色菜单权限.gif


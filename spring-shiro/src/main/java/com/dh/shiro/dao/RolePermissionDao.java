package com.dh.shiro.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.shiro.entity.RolePermission;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:12:29 
 * @Description 类说明 :
 */
public interface RolePermissionDao extends JpaRepository<RolePermission, String>, JpaSpecificationExecutor<RolePermission> {

	List<RolePermission> findByRid(String rid);

}

package com.dh.shiro.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.shiro.entity.Permission;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午1:46:38 
 * @Description 类说明 :
 */
public interface PermissionDao extends JpaRepository<Permission, String>, JpaSpecificationExecutor<Permission> {

}

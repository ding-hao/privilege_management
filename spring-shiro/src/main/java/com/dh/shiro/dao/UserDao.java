package com.dh.shiro.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.shiro.entity.User;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午1:44:26 
 * @Description 类说明 :
 */
public interface UserDao extends JpaRepository<User, String>, JpaSpecificationExecutor<User> {

	User findByUsername(String username);

}

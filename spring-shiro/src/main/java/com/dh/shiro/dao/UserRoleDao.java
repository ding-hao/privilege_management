package com.dh.shiro.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.shiro.entity.UserRole;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:13:14 
 * @Description 类说明 :
 */
public interface UserRoleDao extends JpaRepository<UserRole, String>, JpaSpecificationExecutor<UserRole> {

	List<UserRole> findByUid(String uid);

}

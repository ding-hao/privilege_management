package com.dh.shiro.realm;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午3:14:52 
 * @Description 类说明 : AuthenticationToken token, AuthenticationInfo info
 * 的校验规则
 */
public class CredentialsMatcher extends SimpleCredentialsMatcher {

	@Override
	public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {

		UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken)token;
		// 登录的密码
		String password = new String(usernamePasswordToken.getPassword());
		// 数据库的密码
		String dbPassword = (String)info.getCredentials();
		//自定义校验规则
		return this.equals(password,dbPassword);
	}

}

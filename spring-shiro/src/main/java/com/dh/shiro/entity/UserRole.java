package com.dh.shiro.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:11:04 
 * @Description 类说明 :
 */
@Table(name = "UserRole")
@Entity
public class UserRole implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String uid;
	private String rid;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	
}

package com.dh.shiro.entity; 
/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午11:52:55 
 * @Description 类说明 :
 */

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "Role")
@Entity
public class Role implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	private String rid;
	
	private String name;
	
	
	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

package com.dh.shiro.entity; 
/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午11:52:47 
 * @Description 类说明 :
 */

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "User")
@Entity
public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	private String uid;
	
	private String username;
	
	private String password;
	
	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}

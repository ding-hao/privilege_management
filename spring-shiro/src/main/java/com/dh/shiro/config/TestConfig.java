package com.dh.shiro.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.dh.shiro.entity.User;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年12月3日 下午2:35:59 
 * @Description 类说明 :
 */
@Configuration
public class TestConfig {

	@Bean
	@Primary
	public User defaultUser() {
		User user = new User();
		user.setUsername("admin");
		user.setPassword("123456hahahahaha");
		return user;
	}
	
	
	@Bean
	public User testUser() {
		User user = new User();
		user.setUsername("test");
		user.setPassword("test");
		return user;
	}
	
	@Bean
	public User rootUser() {
		User user = new User();
		user.setUsername("root");
		user.setPassword("root");
		return user;
	}
}

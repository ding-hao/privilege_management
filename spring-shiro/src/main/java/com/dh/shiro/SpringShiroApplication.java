package com.dh.shiro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.spring4all.swagger.EnableSwagger2Doc;

@SpringBootApplication
@EnableSwagger2Doc
public class SpringShiroApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringShiroApplication.class, args);
	}
}

package com.dh.shiro.sevice;

import com.dh.shiro.entity.User;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:02:40 
 * @Description 类说明 :
 */
public interface UserService {

	User findByUsername(String username);

}

package com.dh.shiro.sevice.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dh.shiro.dao.RoleDao;
import com.dh.shiro.dao.UserRoleDao;
import com.dh.shiro.entity.Role;
import com.dh.shiro.entity.UserRole;
import com.dh.shiro.sevice.UserRoleService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:27:50 
 * @Description 类说明 :
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleDao userRoleDao;
	@Autowired
	private RoleDao roleDao;
	
	@Override
	public List<Role> findByUid(String uid) {
		List<Role> roleList = new ArrayList<Role>();
		List<UserRole> ridList = userRoleDao.findByUid(uid);
		if(!CollectionUtils.isEmpty(ridList)) {
			for (UserRole userRole : ridList) {
				roleList.add(roleDao.findById(userRole.getRid()).get());
			}
			return roleList;
		}
		return null;
	}

}

package com.dh.shiro.sevice.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dh.shiro.dao.UserDao;
import com.dh.shiro.entity.User;
import com.dh.shiro.sevice.UserService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:03:20 
 * @Description 类说明 :
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Override
	public User findByUsername(String username) {
		return userDao.findByUsername(username);
	}

}

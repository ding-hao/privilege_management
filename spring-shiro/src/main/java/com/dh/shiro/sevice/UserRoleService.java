package com.dh.shiro.sevice;

import java.util.List;

import com.dh.shiro.entity.Role;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:27:28 
 * @Description 类说明 :
 */
public interface UserRoleService {

	List<Role> findByUid(String uid);

}

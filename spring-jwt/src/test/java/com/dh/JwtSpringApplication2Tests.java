package com.dh;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alibaba.fastjson.JSON;
import com.dh.jwt.auth.BaseTransferEntity;
import com.dh.jwt.dto.SimpleObject;
import com.dh.jwt.utils.MD5Util;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtSpringApplication2Tests {

	/**
	 * 
	 * @Title: contextLoads
	 * @author  
	 * @Description: 客户端将请求数据加密后发给后台的数据信息
	 * @return void
	 */
	@Test
	public void contextLoads() {
        String salt = "t7b3uk";
        
        SimpleObject simpleObject = new SimpleObject();
        simpleObject.setUser("dinghao");
        String md5 = MD5Util.encrypt(JSON.toJSONString(simpleObject) + salt);
        
        BaseTransferEntity baseTransferEntity = new BaseTransferEntity();
        baseTransferEntity.setObject(simpleObject);
        baseTransferEntity.setSign(md5);
        
        System.out.println(JSON.toJSON(baseTransferEntity));
        
        System.out.println("测试加密:"+MD5Util.encrypt("dinghao"));
	}

}

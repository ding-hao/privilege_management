package com.dh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.spring4all.swagger.EnableSwagger2Doc;

@SpringBootApplication
@EnableSwagger2Doc
public class JwtSpringApplication extends WebMvcConfigurationSupport {

	public static void main(String[] args) {
		SpringApplication.run(JwtSpringApplication.class, args);
	}
}

package com.dh.jwt.enums;

/**
 * 
 * @ClassName: BizExceptionEnum
 * @Description: 所有业务异常的枚举
 * @author dinghao
 * @date 2018年11月30日 上午10:10:51
 *
 */
public enum BizExceptionEnum {

	/**
	 * 其他
	 */
	WRITE_ERROR(500,"渲染界面错误"),
	
	/**
	 * token异常
	 */
	TOKEN_EXPIRED(700, "token过期"), TOKEN_ERROR(700, "token验证失败"),

	/**
	 * 签名异常
	 */
	SIGN_ERROR(700, "签名验证失败"),

	/**
	 * 其他
	 */
	AUTH_REQUEST_ERROR(400, "账号密码错误");

	BizExceptionEnum(int code, String message) {
		this.friendlyCode = code;
		this.friendlyMsg = message;
	}

	private int friendlyCode;

	private String friendlyMsg;

	public int getCode() {
		return friendlyCode;
	}

	public void setCode(int code) {
		this.friendlyCode = code;
	}

	public String getMessage() {
		return friendlyMsg;
	}

	public void setMessage(String message) {
		this.friendlyMsg = message;
	}

}

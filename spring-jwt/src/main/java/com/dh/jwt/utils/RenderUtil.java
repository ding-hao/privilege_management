package com.dh.jwt.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.dh.jwt.enums.BizExceptionEnum;
import com.dh.jwt.exception.BussinessException;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 上午10:38:54
 * @Description 类说明 :
 */
public class RenderUtil {

	/**
	 * 渲染json对象
	 */
	public static void renderJson(HttpServletResponse response, Object jsonObject) {
		try {
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			writer.write(JSON.toJSONString(jsonObject));
		} catch (IOException e) {
			throw new BussinessException(BizExceptionEnum.WRITE_ERROR);
		}
	}

}

package com.dh.jwt.dto;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 上午10:51:55
 * @Description 类说明 :
 */
public class SimpleObject {

	private String user;

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}
}

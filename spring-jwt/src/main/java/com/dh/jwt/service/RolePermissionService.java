package com.dh.jwt.service;

import java.util.List;

import com.dh.jwt.entity.Permission;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:38:58 
 * @Description 类说明 :
 */
public interface RolePermissionService {

	List<Permission> findByRid(String rid);

}

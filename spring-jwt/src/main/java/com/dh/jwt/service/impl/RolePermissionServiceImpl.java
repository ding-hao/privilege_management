package com.dh.jwt.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.dh.jwt.dao.PermissionDao;
import com.dh.jwt.dao.RolePermissionDao;
import com.dh.jwt.entity.Permission;
import com.dh.jwt.entity.RolePermission;
import com.dh.jwt.service.RolePermissionService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:39:40 
 * @Description 类说明 :
 */
@Service
public class RolePermissionServiceImpl implements RolePermissionService {

	@Autowired
	private RolePermissionDao rolePermissionDao;
	
	@Autowired
	private PermissionDao permissionDao;
	
	@Override
	public List<Permission> findByRid(String rid) {
		List<Permission> permissionList = new ArrayList<>();
		List<RolePermission> rolePermissionList = rolePermissionDao.findByRid(rid);
		if(!CollectionUtils.isEmpty(rolePermissionList)) {
			for (RolePermission rolePermission : rolePermissionList) {
				permissionList.add(permissionDao.findById(rolePermission.getPid()).get());
			}
			return permissionList;
		}
		return null;
	}

}

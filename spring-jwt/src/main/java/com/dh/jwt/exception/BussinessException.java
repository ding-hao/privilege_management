package com.dh.jwt.exception;

import com.dh.jwt.enums.BizExceptionEnum;

/**
 * @author fengshuonan
 * @Description 业务异常的封装
 * @date 2016年11月12日 下午5:05:10
 */
public class BussinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	//友好提示的code码
	protected int friendlyCode;

	//友好提示
	protected String friendlyMsg;

	//业务异常跳转的页面
	protected String urlPath;
	
	public BussinessException(BizExceptionEnum bizExceptionEnum) {
//		super(bizExceptionEnum.getMessage());
		this(bizExceptionEnum.getCode(), bizExceptionEnum.getMessage(), "");
	}
	
	protected BussinessException(int friendlyCode, String friendlyMsg, String urlPath) {
		this.setValues(friendlyCode, friendlyMsg, urlPath);
	}
	
	private void setValues(int friendlyCode, String friendlyMsg, String urlPath) {
		this.friendlyCode = friendlyCode;
		this.friendlyMsg = friendlyMsg;
		this.urlPath = urlPath;
	}
	
	public int getCode() {
		return friendlyCode;
	}

	public void setCode(int code) {
		this.friendlyCode = code;
	}

	public String getMessage() {
		return friendlyMsg;
	}

	public void setMessage(String message) {
		this.friendlyMsg = message;
	}

	public String getUrlPath() {
		return urlPath;
	}

	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}
}

package com.dh.jwt.warpper;

import java.util.Map;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 下午5:35:29 
 * @Description 类说明 :
 */
public class UserWarpper extends BaseControllerWarpper{
	
	public UserWarpper(Object list) {
		super(list);
	}


	@Override
	protected void warpTheMap(Map<String, Object> map) {
		String username = (String)map.get("username");
		map.put("sex", "男");
		map.put("username", username);
	}

}

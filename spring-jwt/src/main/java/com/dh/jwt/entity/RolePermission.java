package com.dh.jwt.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 下午2:08:05 
 * @Description 类说明 :
 */
@Table(name = "RolePermission")
@Entity
public class RolePermission implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	private String id;
	private String rid;
	private String pid;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	public String getPid() {
		return pid;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	
}

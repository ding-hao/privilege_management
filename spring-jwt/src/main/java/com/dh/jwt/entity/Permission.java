package com.dh.jwt.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午11:53:06 
 * @Description 类说明 :
 */
@Table(name = "Permission")
@Entity
public class Permission implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	private String pid;
	
	private String name;
	
	private String url;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
}

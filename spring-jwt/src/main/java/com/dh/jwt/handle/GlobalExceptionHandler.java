package com.dh.jwt.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dh.jwt.auth.tip.ErrorTip;
import com.dh.jwt.enums.BizExceptionEnum;
import com.dh.jwt.exception.BussinessException;

import io.jsonwebtoken.JwtException;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月30日 下午5:20:35 
 * @Description 类说明 :
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 拦截jwt相关异常
     */
    @ExceptionHandler(JwtException.class)
    @ResponseBody
    public ErrorTip jwtException(JwtException e) {
        return new ErrorTip(BizExceptionEnum.TOKEN_ERROR.getCode(), BizExceptionEnum.TOKEN_ERROR.getMessage());
    }
    
    @ExceptionHandler(BussinessException.class)
    @ResponseBody
    public ErrorTip bussinessException(BussinessException e) {
    	log.error("业务异常:", e);
        return new ErrorTip(e.getCode(), e.getMessage());
    }
    
}

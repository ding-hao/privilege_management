package com.dh.jwt.controller;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dh.jwt.dto.SimpleObject;
import com.dh.jwt.entity.User;
import com.dh.jwt.utils.BeanKit;
import com.dh.jwt.warpper.UserWarpper;

/**
 * 常规控制器
 *
 * @author fengshuonan
 * @date 2017-08-23 16:02
 */
@RestController
@RequestMapping("/hello")
public class ExampleController extends BaseController{

    @RequestMapping("")
    public ResponseEntity<String> hello(@RequestBody SimpleObject simpleObject) {
        System.out.println(simpleObject.getUser());
        
        return ResponseEntity.ok("请求成功!");
    }
    
    @GetMapping("/user")
    public Object list(String condition) {
        User user = new User();
        user.setUsername("张三");
        Map<String, Object> beanToMap = BeanKit.beanToMap(user);
        return super.warpObject(new UserWarpper(beanToMap));
    }

}

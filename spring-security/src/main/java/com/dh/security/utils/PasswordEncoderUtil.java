package com.dh.security.utils;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午9:35:46 
 * @Description 类说明 : 自定义加密算法(使用BCryptPasswordEncoder加密)
 */
public class PasswordEncoderUtil implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.encode(rawPassword);
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder.matches(rawPassword, encodedPassword);
	}

}

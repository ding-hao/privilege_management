package com.dh.security.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午9:27:27 
 * @Description 类说明 : 实现数据库管理登录用户
 */
public interface UserService extends UserDetailsService{

}

package com.dh.security.service.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.dh.security.service.UserService;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月26日 上午9:27:59 
 * @Description 类说明 : 实现数据库管理登录用户
 */
@Service
public class UserServiceImpl implements UserService{

	/**
	 * 实现UserDetailsService的loadUserByUsername方法
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return null;
	}

}

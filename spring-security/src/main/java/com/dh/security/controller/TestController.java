package com.dh.security.controller;

import java.util.List;

import org.apache.catalina.User;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 作者 dinghao
 * @version 创建时间：2018年11月23日 下午5:20:15
 * @Description 类说明 :
 */
@RestController
public class TestController {

	@RequestMapping("/hello")
	public String hello() {
		return "hello spring-security";
	}

	@RequestMapping("/")
	public String helloWorld() {
		return "hello spring-security";
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping("/role/admin")
	public String roleAdmin() {
		return "需要admin角色";
	}

	@PreAuthorize("hasRole('ROLE_GUEST')")
	@RequestMapping("/role/guest")
	public String roleGuest() {
		return "需要guest角色";
	}

	// 调用前
	// @PreAuthorize("hasRole('ROLE_GUEST') or hasRole('ROLE_ADMIN') or
	// hasAuthority('') or hasPermission()")
	@PreAuthorize("#id<10 and principal.username.equals(#username) and user.username.equals('abc')")
	// 调用后
	@PostAuthorize("returnObject%2==0")
	@RequestMapping("/test")
	public Integer test(Integer id, String username, User user) {
		return id;
	}

	//对集合处理
	@PreFilter("filterObject%2==0")
	@PostFilter("filterObject%4==0")
	@RequestMapping("/test2")
	public List<Integer> test2(List<Integer> idList) {
		return idList;
	}

}

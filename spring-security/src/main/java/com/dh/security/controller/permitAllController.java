package com.dh.security.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月23日 下午5:22:35 
 * @Description 类说明 :
 */
@RestController
@RequestMapping("/permitAll")
public class permitAllController {

	@RequestMapping("/home")
	public String home() {
		return "hello spring-security";
	}
	
}

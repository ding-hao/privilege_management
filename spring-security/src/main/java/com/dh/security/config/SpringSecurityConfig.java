package com.dh.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.dh.security.service.UserService;
import com.dh.security.utils.PasswordEncoderUtil;

/**
 * 
 * @ClassName: SpringSecurityConfig
 * @Description: 开启Security的web配置
 * @author dinghao
 * @date 2018年11月23日 下午5:15:29
 *
 */
@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userService;
	
	/**
	 * 忽略静态资源
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/js/**", "/css/**", "/images/**");
	}

	/**
	 * 配置请求拦截
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		//设置登录,注销，表单登录不用拦截，其他请求要拦
		http.authorizeRequests().antMatchers("/permitAll/**").permitAll().
				anyRequest().authenticated().
				and()
				.logout().permitAll()
				.and().formLogin();
		//开启跨域
		http.csrf();
	}

	/**
	 * 基于内存的验证(需求: 只要登录即可(内部使用))
	 * 默认账户密码
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		/**
		 * 1.使用内置用户
		 */
//		// 可以设置内存指定的登录的账号密码,指定角色
//      // sprinboot 2.0 以后 不加.passwordEncoder(new MyPasswordEncoder())
//      // 就不是以明文的方式进行匹配，会报错
//		// 可以添加多个用户
		/**
		 * 指定角色,每个角色指定权限
		 */
//		auth.inMemoryAuthentication().passwordEncoder(new MyPasswordEncoder()).withUser("admin").password("123456").roles("ADMIN");
//		auth.inMemoryAuthentication().passwordEncoder(new MyPasswordEncoder()).withUser("guest").password("123456").roles("GUEST");
		
		/**
		 * 2.使用数据库管理用户
		 */
		//使用数据库管理用户
		auth.userDetailsService(userService).passwordEncoder(new PasswordEncoderUtil());
//		//默认的数据库验证
//		auth.jdbcAuthentication().usersByUsernameQuery("").authoritiesByUsernameQuery("").passwordEncoder(new BCryptPasswordEncoder());
	}
	
}

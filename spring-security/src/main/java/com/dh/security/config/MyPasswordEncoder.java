package com.dh.security.config;

import org.springframework.security.crypto.password.PasswordEncoder;

/** 
 * @author 作者 dinghao
 * @version 创建时间：2018年11月23日 下午5:38:49 
 * @Description 类说明 : 对密码进行加密解密(只是测试功能utils包中存在具体加密算法)
 */
public class MyPasswordEncoder implements PasswordEncoder {

	@Override
	public String encode(CharSequence rawPassword) {
		return rawPassword.toString();
	}

	@Override
	public boolean matches(CharSequence rawPassword, String encodedPassword) {
		return encodedPassword.equals(rawPassword.toString());
	}

}
